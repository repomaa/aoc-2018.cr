pairs = 0
triplets = 0

STDIN.each_line(chomp: true) do |line|
  pair_found = false
  triplet_found = false

  line.each_char.uniq.each do |char|
    case line.count(char)
    when 2 then pair_found = true
    when 3 then triplet_found = true
    end

    break if pair_found && triplet_found
  end

  pairs += 1 if pair_found
  triplets += 1 if triplet_found
end

puts pairs * triplets
